<?php
class RecipeManager
{
    private $db;

    public function __construct()
    {
        $dbName = "cook'in";
        $port = 3306;
        $userName = "root";
        $password = "root";
        try {
            $this->setDb(new PDO("mysql:host=localhost;dbname=$dbName;port=$port;charset=utf8mb4",$userName, $password));
        } catch(PDOException $error) {
            echo $error->getMessage();
        }
    }

    public function setDb($db) {
        $this->db = $db;
    }

    public function add (Recipe $recipe) {
        $req = $this->db->prepare("INSERT INTO `recipe` (title, ingredient,content, author_id) VALUES (:title, :ingredient, :content, :author_id)");
        $req->bindValue(":title", $recipe->getTitle(), PDO::PARAM_STR);
        $req->bindValue(":ingredient", $recipe->getIngredient(), PDO::PARAM_STR);
        $req->bindValue(":content", $recipe->getContent(), PDO::PARAM_STR);
        $req->bindValue(":author_id", $recipe->getAuthor_id(), PDO::PARAM_INT);
        
        $req->execute();
    }
    public function get(int $id) {

    }

    public function getAll() {

    }

    public function update(RecipeManager $recipe) {

    }
    public function delete(int $id) {
        
    }
} 
?>
